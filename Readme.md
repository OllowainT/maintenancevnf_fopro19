# Readme for maintenancevnf tool

This is a small cli-tool to build a kvm-based environment. Hypervisor and Guests are all Ubuntu 18.10+ based.
maintenancevnf can setup the test environment, clone a VM in live mode to a Maintenance VM, set VM to
maintenance mode while starting the Maintenance-VM to serve the VNF. It takes care about passthrough
Devices.

This Tool is highly specialiced and is used to give a PoC for maintaining VNFs without a long service outage.
Some vars are hardcoded, yet...

## Setup
```
git clone git@gitlab.com:OllowainT/maintenancevnf_fopro19.git
```
go to <repo-dir>/vnf-handover
```
python3 setup.py develop
```
or run
```
python3 setup.py install
```

## How to run

After that you can run
```
maintenancevnf --help
```
to get additional information or
```
maintenancevnf COMMAND --help
```
to get command-specific help.


## How to run in defined order
Steps to run:

maintenancevnf

1. build-test          --> setup initial test environment
2. clone-live-vnf      --> clone live-vm and generate maintenance vm
3. define-maintenance  --> generate maintenance domain description
4. prep-maintenance-py --> prepare maintenance vm via python script
5. handover            --> set live-vm to passive and maintenance vm to active
6. commit              --> give back control to live vm.
99. testconn           --> test connection to kvm hypervisor


## Hypervisor in this Testcase:
VMWare Workstation based Ubuntu 18.10LTS (Nested-Virtualization)
Memory: 8GB
Processors: 4
HDD: 60GB
Paravirtual NIC: e1000
Passthrough-NIC: vmxnet


## Measure values:

stress tester used on hypervisor:
stress-ng version. 0.09.25
command: stress-ng -c 3 -l 30

with memory stress 75%
(stress-ng -c 3 -l 30 --vm 1 --vm-bytes 60% --io 3 --vm-method all --metrics-brief)


iperf version: 3.1.3
iperf server started on test-client

iperf client started on externet-vm with following params:
iperf3 -c 172.16.1.2 -t 10 -u -b 50M/10




no load on hypervisor:
packet loss: 0/7570

no handover:
cpu stress 30%
Jitter: 3.792, 4.025, 4.196, 4.061, 4.872, 3.958, 4.146, 4.997, 4.031, 4.122
packet loss: 3, 38, 13, 6, 12, 14, 0, 24, 5, 10

no handover:
cpu stress 30%, io stress 3, memory stress 60%
packet loss: 1278, 1614, 1777, 1179, 1108, 1546, 1330, 2354, 1968, 1956
_____________

with handover:
no cpu stress
packet loss: 1627, 1628, 1456, 1612, 1465, 1454, 1635, 1450, 1476, 1625


with handover:
cpu stress 30%
packet loss: 1698, 1467, 1691, 1524, 1510, 1689, 1581, 1650, 1533, 1680

with handover:
cpu stress 30%, io stress 3, memory stress 60%
packet loss: 2440, 2362, 2775, 2517, 2387, 3009, 3017, 3166, 2793, 2505
