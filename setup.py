# from setuptools import setup, find_packages

# helps installing on every environment and loads dependencies
# to install vnf-handover run: python3 setup.py install
# to develop on these program better run: python3 setup.py develop
# for more help to change this setup see: https://click.palletsprojects.com/en/7.x/setuptools/
try:
    from setuptools import setup, find_packages
except ImportError:
    from distutils.core import setup, find_packages

setup(
    name='maintenancevnf',
    version='0.1',
    packages=find_packages(),
    include_package_data=True,
    install_requires=[
        'Click',
        'libvirt-python',
        'pyfiglet',
        'python-hwinfo',
        'sysrsync',
        'pexpect',
    ],
    entry_points= """
        [console_scripts]
        maintenancevnf = maintenancevnf.cli:cli
    """,
)
