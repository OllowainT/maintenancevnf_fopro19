import libvirt
import sys
import click


class Qemu_Connection(object):

    def __init__(self, user, passwd):
        print("init qemu_connection object")
        self.user = user
        self.passwd = passwd
        self.conn = None
        self.qemu_hostname = None

    def __enter__(self):
        print("entered Qemu_Connection")
        def request_cred(credentials, user_data):
            for credential in credentials:
                if credential[0] == libvirt.VIR_CRED_AUTHNAME:
                    credential[4] = self.user
                elif credential[0] == libvirt.VIR_CRED_PASSPHRASE:
                    credential[4] = self.passwd
            return 0

        auth = [[libvirt.VIR_CRED_AUTHNAME, libvirt.VIR_CRED_PASSPHRASE],
            request_cred, None]
        print("before conn")
        self.conn = libvirt.openAuth("qemu:///system", auth, 0)
        if self.conn is None:
            print('Failed to open connection', file=sys.stderr)
            return False
            exit(1)

        click.echo("connection to qemu established...\n")
        hostname = self.conn.getHostname()
        self.qemu_hostname = hostname
        return self

    def __exit__(self, exception_type, exception_value, traceback):
        self.conn.close()
        print("QEMU connection closed")
