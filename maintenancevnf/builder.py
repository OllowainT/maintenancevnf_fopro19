import os
import sys
import subprocess



class Builder(object):

    def __init__(self, conn):
        self.name = "builder"
        self.conn = conn

    def unbind_pci(self):
        subprocess.run(["echo","'15ad 07b0'", ">", "/sys/bus/pci/drivers/pci-stub/new_id" ])
        subprocess.run(["echo","'0000:03:00.0'", ">", "/sys/bus/pci/devices/0000:03:00.0/driver/unbind" ])
        subprocess.run(["echo","'0000:03:00.0'", ">", "/sys/bus/pci/drivers/pci-stub/bind" ])
        subprocess.run(["virsh","nodedev-detach", "pci_0000_03_00_0"])

    def ovs_net(self):

        with open("/opt/management/vnf-handover/maintenancevnf/xmldescriptors/ovs0.xml") as lanxml:

            try:
                network = self.conn.networkLookupByName('ovs0')
                if network.isActive():
                    network.destroy()
                if network.isPersistent():
                    network.undefine()
            except:
                print("ovs0 not found")

            lswitch = lanxml.read()
            swdom = self.conn.networkDefineXML(lswitch)
            if swdom is None:
                print('Failed to create a network from an XML definition.', file=sys.stderr)
                exit(1)
            swdom.create()
            if swdom.isActive():
                swdom.setAutostart(1)
                print('The new persistent virtual network is active and set to autostart')
                rcode = subprocess.call("ovs-vsctl add-br ovs0", shell=True)

            else:
                print('The new persistent virtual network is not active')




    def live_vm(self):
        with open("/opt/management/vnf-handover/maintenancevnf/xmldescriptors/live_router_vnf.xml") as live_router_xml:

            try:
                dom = self.conn.lookupByName("live-router")
                if dom.isActive():
                    dom.destroy()
                if dom.isPersistent():
                    dom.undefine()
            except:
                print("live-router not found")

            live_vnf = live_router_xml.read()
            domain = self.conn.defineXML(live_vnf)
            if domain == None:
                print('Failed to create a domain from an XML definition.', file=sys.stderr)
                exit(1)
            domain.create()
            if domain.isActive():
                domain.setAutostart(1)
                print('The new persistent virtual domain is active and set to autostart')
                print('Guest '+domain.name()+' has booted')
            else:
                print('The new persistent virtual domain is not active')

    def test_client(self):
        with open("/opt/management/vnf-handover/maintenancevnf/xmldescriptors/test-client.xml") as test_client_xml:

            try:
                dom = self.conn.lookupByName("test-client")
                if dom.isActive():
                    dom.destroy()
                if dom.isPersistent():
                    dom.undefine()
            except:
                print("test-client not found")

            test_client = test_client_xml.read()
            domain = self.conn.defineXML(test_client)
            if domain == None:
                print('Failed to create a domain from an XML definition.', file=sys.stderr)
                exit(1)
            domain.create()
            if domain.isActive():
                domain.setAutostart(1)
                print('The new persistent virtual domain is active and set to autostart')
                print('Guest '+domain.name()+' has booted')
            else:
                print('The new persistent virtual domain is not active')
