import xml.etree.ElementTree as ET
import os
import subprocess
import libvirt
from xml.dom import minidom
import uuid
from hwinfo.pci import PCIDevice
from hwinfo.pci.lspci import LspciNNMMParser

#returns path to
class Maintenance_Configurator(object):
    def __init__(self, live_vnf_xml, vnf_name, qemu_connection, live_img_path):
        self.name = "Maintenance_Configurator"
        self.conn = qemu_connection
        self.live_vnf_name = vnf_name
        self.live_image_path = live_img_path
        self.maintenance_image_path = None
        self.live_vnf_xml_path = live_vnf_xml
        self.maintenance_vnf_xml_path = None
        self.maintenance_vnf_config_path = None
        self.live_tree = None
        self.test = None
        self.passthrough_device_name = None
        self.interface_device_name = None

    def __enter__(self):
        self._dump_live()
        self.live_tree = ET.parse(self.live_vnf_xml_path)
        self._set_maintenance_xml_path()
        self._set_maintenance_config_path()
        self._set_maintenance_image_path()
        self._init_maintenance_config()
        self._init_maintenance_xml()
        return self

    def __exit__(self, exception_type, exception_value, traceback):
        print("exit Maintenance Configurator")
        self._write_to_maintenance_xml()
        self.test = self._selftest()
        if self.test:
            return True
        else:
            return False

    def _selftest(self):
        if self.maintenance_image_path is None:
            return False
        if self.maintenance_vnf_xml_path is None:
            return False
        if self.maintenance_vnf_config_path is None:
            return False
        if (ET.parse(self.maintenance_vnf_config_path).getroot().find(".//passthrough_devices/hostdev") is None):
            return False
        if (ET.parse(self.maintenance_vnf_config_path).getroot().find(".//interface_devices/interface") is None):
            return False
        else:
            return True



    def _set_maintenance_xml_path(self):
        self.maintenance_vnf_xml_path = str(os.path.dirname(self.live_vnf_xml_path) + "/"
            + os.path.basename(self.live_vnf_xml_path).split(".")[0] + "_maintenance" + ".xml")
        #print("maintenance xml path is " + self.maintenance_vnf_xml_path)

    def _set_maintenance_image_path(self):
        self.maintenance_image_path = str(os.path.dirname(self.live_image_path) + "/"
            + os.path.basename(self.live_image_path).split(".")[0] + "_maintenance" + ".qcow2")
        #print("maintenance image path is " + self.maintenance_image_path)

    def _set_maintenance_config_path(self):
        self.maintenance_vnf_config_path = str(os.path.dirname(self.live_vnf_xml_path) + "/"
            + os.path.basename(self.live_vnf_xml_path).split(".")[0] + "_maintenance" + ".config")
        #print("maintenance config path is " + self.maintenance_vnf_config_path)

    def _init_maintenance_config(self):
        root = ET.Element("maintenance-config")
        ET.SubElement(root, "interface_devices")
        ET.SubElement(root, "passthrough_devices")
        maintenance_tree = ET.ElementTree(root)
        maintenance_tree.write(self.maintenance_vnf_config_path)

    def _dump_live(self):
        print("dump live vnf xml")
        live_vm = self.conn.lookupByName(self.live_vnf_name)
        raw_live_xml = live_vm.XMLDesc(0)
        with open(self.live_vnf_xml_path, "w") as lxml:
            lxml.write(raw_live_xml)

    def _init_maintenance_xml(self):
        #get original xml config of live vm:
        live_tree_temp = ET.parse(self.live_vnf_xml_path)
        live_tree_root = live_tree_temp.getroot()

        # adjust original
        # change Name
        name = live_tree_root.find(".//name")
        name.text = self.live_vnf_name + "-maintenance"

        # change kvm id
        id = live_tree_root.get("id")
        if id is not None:
            newid = int(id) + 1
            live_tree_root.set('id', '%s' % str(newid))

        # change uuid
        newuuid = uuid.uuid1()
        #print(newuuid)
        orig_uuid = live_tree_root.find(".//uuid")
        orig_uuid.text = str(newuuid)
        # change disk path
        disk_path = live_tree_root.find(".//devices/disk/source")
        disk_path.set('file', '%s' % self.maintenance_image_path)
        # write adjusted maintenance xml
        live_tree_temp.write(self.maintenance_vnf_xml_path)

    def add_interface_device(self, device_name):
        self.interface_device_name = device_name
        #get original xml config of live vm:
        live_tree_temp = ET.parse(self.live_vnf_xml_path)
        live_tree_root = live_tree_temp.getroot()
        interface_device = live_tree_root.find(".//interface/alias[@name='%s']/.." % device_name)

        #append to config file:
        maintenance_tree = ET.parse(self.maintenance_vnf_config_path)
        mroot = maintenance_tree.getroot()
        inputpoint = mroot.find('interface_devices')
        inputpoint.append(interface_device)
        maintenance_tree.write(self.maintenance_vnf_config_path)


    def add_passthrough_device(self, passthrough_device):
        self.passthrough_device_name = passthrough_device
        #get original xml config of live vm:

        live_tree_temp = ET.parse(self.live_vnf_xml_path)
        live_tree_root = live_tree_temp.getroot()
        passthrough_dev = live_tree_root.find(".//hostdev/alias[@name='%s']/.." % passthrough_device)

        #append to config file:
        maintenance_tree = ET.parse(self.maintenance_vnf_config_path)
        mroot = maintenance_tree.getroot()
        inputpoint = mroot.find('passthrough_devices')
        inputpoint.append(passthrough_dev)
        maintenance_tree.write(self.maintenance_vnf_config_path)
        # get info out of device for attaching/dettaching
        self.get_pci_info(passthrough_device)

    def _write_to_maintenance_xml(self):
        tree_temp_root = ET.parse(self.maintenance_vnf_config_path).getroot()
        ET.SubElement(tree_temp_root, "live_vnf_name")
        ET.SubElement(tree_temp_root, "live_image_path")
        ET.SubElement(tree_temp_root, "maintenance_image_path")
        ET.SubElement(tree_temp_root, "live_vnf_xml_path")
        ET.SubElement(tree_temp_root, "maintenance_vnf_xml_path")
        ET.SubElement(tree_temp_root, "interface_device_name")
        ET.SubElement(tree_temp_root, "passthrough_device_name")

        # set params
        tree_temp_root.find("live_vnf_name").text = self.live_vnf_name
        tree_temp_root.find("live_image_path").text = self.live_image_path
        tree_temp_root.find("maintenance_image_path").text = self.maintenance_image_path
        tree_temp_root.find("live_vnf_xml_path").text = self.live_vnf_xml_path
        tree_temp_root.find("maintenance_vnf_xml_path").text = self.maintenance_vnf_xml_path
        tree_temp_root.find("interface_device_name").text = self.interface_device_name
        tree_temp_root.find("passthrough_device_name").text = self.passthrough_device_name

        maintenance_tree = ET.ElementTree(tree_temp_root)

        maintenance_tree.write(self.maintenance_vnf_config_path)

    # gets actually only one device
    def get_pci_info(self, passthrough_device):
        print("todo: implement")
        # get device:
        live_tree_temp = ET.parse(self.maintenance_vnf_config_path)
        live_tree_root = live_tree_temp.getroot()
        pci_add = live_tree_root.find(".//passthrough_devices/hostdev/alias[@name='%s']/../source/address" % passthrough_device)
        ET.dump(pci_add)
        domain = pci_add.get('domain')
        bus = pci_add.get('bus')
        function = pci_add.get('function')
        slot = pci_add.get('slot')

        print(domain, bus, function, slot)
