import pexpect
import sys
import datetime

act_date = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")

child = pexpect.spawn('virsh console live-router-maintenance', encoding='utf-8')
print(child.read())
child.expect('.*]')
child.sendline('\r')
li = child.expect(['.*login: ', '.*~.*'])
if li == 0:
    child.sendline('root')
    child.expect('.*assword:.*')
    child.sendline('password1!')
    child.sendline('\r')
child.expect('.*~.*')
# we are logged in now
child.sendline('date --set %s' % act_date)
child.expect(r'\d\d\d\d$')
child.close(force=True)
