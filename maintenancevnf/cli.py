#!/usr/bin/env python3

import click
import yaml
from threading import Thread
import threading
import time
import subprocess
import os
import sys
import libvirt
from pyfiglet import Figlet
import xml.dom.minidom as minidom
import xml.etree.ElementTree as ET
import shlex

# own modules
from maintenancevnf.qemu_connection import Qemu_Connection
from maintenancevnf.builder import Builder
from maintenancevnf.cloner import *


# global vars:
QEMU_USER = 'root'
QEMU_PW = 'password1!'


#########################
# cli commands - takes commands and run normal methods
#########################


# entry point --> see setup.py
@click.group()
def cli():
    f = Figlet(font='slant')
    click.echo(f.renderText('maintenancevnf'))
    click.echo(
    """
    Info here
    """
    )
    pass


@cli.command()
@click.option('-p', '--password', hide_input=True, required=True, help="send password for authentication", default=QEMU_PW)
@click.option('-u', '--user', required=True, help="define user to connect", default="root")
def testconn(password, user):
    """test connection to kvm hypervisor"""
    connectqemu(password=password, user=user)

# this is just to bootstrap the test environment
@cli.command()
def build_test():
    """1 - setup initial test environment"""
    click.echo("start building test network and test vms")
    start_test()

@cli.command()
@click.option('-lp', '--live-image-path', required=True, help="define live image path", default="/datastore/iso/live_router.qcow2")
@click.option('--use-old-image', help="if defined, use an existent image", is_flag=True)
@click.option('-p', '--passthrough-device', help="define the name of a passthrough device (alias)", default="ua-pass0")
@click.option('-i', '--interface-device', help="define the name of a interface device (alias)", default="ua-lan0")
@click.option('-n', '--name', required=True, help='name of the vm', default='live-router')
def clone_live_vnf(live_image_path, use_old_image, passthrough_device, interface_device, name):
    """2 - clone live-vm and generate maintenance vm"""
    click.echo("start cloning process")
    clone(live_image_path, use_old_image, name, interface_device, passthrough_device)

@cli.command()
@click.option('-c', '--config-path', required=True, help="define config path", default="/datastore/iso/live_router_maintenance.config")
def define_maintenance(config_path):
    """3 - generate maintenance domain description"""
    click.echo("Definition phase")
    define_main_xml(config_path)

@cli.command()
@click.option('-n', '--name', required=True, help='name of the vm', default='live-router-maintenance')
@click.argument('scriptpath', type=click.Path(exists=True), default="/opt/management/vnf-handover/maintenancevnf/prepare_maintenance.py")
def prep_maintenance_py(scriptpath, name):
    """4 - prepare maintenance vm via python script and suspend."""
    click.echo("prepare the maintenance VM with defined script...")
    run_maintenance_prep(scriptpath, name)

@cli.command()
@click.option('-n', '--name', required=True, help='name of the vm', default='live-router')
@click.option('-c', '--config-path', required=True, help="define config path", default="/datastore/iso/live_router_maintenance.config")
def handover(name, config_path):
    """5 - set live-vm to passive and maintenance vm to active."""
    click.echo("handover...")
    do_handover(name, config_path)

@cli.command()
@click.option('-n', '--name', required=True, help='name of the vm', default='live-router')
@click.option('-c', '--config-path', required=True, help="define config path", default="/datastore/iso/live_router_maintenance.config")
def commit(name, config_path):
    """6 - give back control to live vm. Handover nic's"""
    click.echo("handover...")
    commit(name, config_path)


#########################
# normal methods
#########################


def connectqemu(**kwargs):
    user = kwargs.get("user", None)
    passwd = kwargs.get("password", None)

    with Qemu_Connection(user, passwd) as qconn:
        print(qconn.qemu_hostname)


def start_test(**kwargs):
    with Qemu_Connection(QEMU_USER, QEMU_PW) as qconn:
        print(qconn.qemu_hostname)
        build = Builder(qconn.conn)
        print(build.name)
        build.unbind_pci()
        build.ovs_net()
        build.live_vm()
        build.test_client()


def clone(live_image_path, use_old_image, name, interface_device, passthrough_device):
    with Qemu_Connection(QEMU_USER, QEMU_PW) as qconn:
        # todo: check if the interface_device and passthrough_device param is given. if not react another way
        with Cloner(live_image_path, use_old_image, name, qconn.conn, interface_device, passthrough_device) as cloner:
            print("live image size: ", cloner.live_image_size)

            try:
                systemcheck = cloner.check_system()
                # initialization phase:
                if systemcheck:
                    print("systemcheck was positive")
                    config_state = cloner.write_config()

                    # copy phase:
                    if config_state:
                        cloning = cloner.clone_image()
                        if cloning:
                            print("cloning was successfull. maintenance path: {maintenance_image_path}".format(maintenance_image_path=cloner.maintenance_image_path))
                    else:
                        print("error in writing maintenance config")
                else:
                    print("systemcheck was negative")


            except SpaceLowException:
                print("Error: no cloning possible. There is not enough space in"
                "your datastore, where the image is located")
            except NoAccessException:
                print("Error: no cloning possible. You have no permissions"
                " to the filesystem")
            except UnsupportedFileException:
                print("Error: file system of live-image not supported")
            except FileNotFoundError:
                print("Error: cant find the specified file")


def define_main_xml(config_path):
    print("check maintenance xml for devices")

    # get maintenance info out of maintenance config
    config = minidom.parse(config_path)
    maintenance_xml_path = config.getElementsByTagName("maintenance_vnf_xml_path")[0].firstChild.data
    passthrough_device = config.getElementsByTagName("passthrough_devices")[0].firstChild
    passthrough_device_name = config.getElementsByTagName("passthrough_device_name")[0].firstChild.data
    interface_device_name = config.getElementsByTagName("interface_device_name")[0].firstChild.data

    print("ndosnds: ", passthrough_device_name)
    with Qemu_Connection(QEMU_USER, QEMU_PW) as qconn:
        new_main_xml = None


        with open(maintenance_xml_path) as xmlfile:
            tree_temp = minidom.parse(xmlfile)
            tree_root = tree_temp.documentElement
            for parent in tree_root.childNodes:
                for child in parent.childNodes:
                    # todo: check for device name - now it works with only one hostdev
                    # and one network interface device. Also there is no name check
                    if child.nodeType is minidom.Node.ELEMENT_NODE:
                        # passthrough device:
                        if(child.tagName == 'hostdev'):
                            parent.removeChild(child)
                        # interface device:
                        if(child.tagName == 'interface'):
                            parent.removeChild(child)

            new_main_xml = tree_root.toxml()

        # write new config
        xmlfile = open(maintenance_xml_path, 'w')
        xmlfile.write(new_main_xml)
        xmlfile.close()

        #define and start maintenance vm:
        with open(maintenance_xml_path, 'r') as xml:
            m_xml = xml.read()
            m_domain = qconn.conn.defineXML(m_xml)
            if m_domain == None:
                print('Failed to create maintenance-vm from XML definition.', file=sys.stderr)
                exit(1)
            else:
                #after running:
                #m_domain.detachDevice(passthrough_device.toxml())
                m_domain.create()
                m_domain.setAutostart(1)


def run_maintenance_prep(scriptpath, name):
    process = subprocess.run(['python3', scriptpath], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    state = process.returncode
    if state < 0:
        print("error while running ", scriptpath)
    else:
        print("Prep script end")

    # suspend guest:
    with Qemu_Connection(QEMU_USER, QEMU_PW) as qconn:
        maintenance_vm = qconn.conn.lookupByName(name)
        if maintenance_vm.isActive():
            if maintenance_vm.suspend() < 0:
                print('Unable to suspend guest', file=sys.stderr)
            else:
                print("maintenance vm suspended")

def do_handover(name, config_path):
    print("detach defined defices")
    config = minidom.parse(config_path)
    maintenance_xml_path = config.getElementsByTagName("maintenance_vnf_xml_path")[0].firstChild.data
    passthrough_device = config.getElementsByTagName("passthrough_devices")[0].firstChild
    passthrough_device_name = config.getElementsByTagName("passthrough_device_name")[0].firstChild.data
    interface_device_name = config.getElementsByTagName("interface_device_name")[0].firstChild.data
    # takes only the first interface right now:
    interface_device = config.getElementsByTagName("interface_devices")[0].firstChild


    with Qemu_Connection(QEMU_USER, QEMU_PW) as qconn:
        live_vm = qconn.conn.lookupByName(name)
        maintenance_vm = qconn.conn.lookupByName(name + "-maintenance")
        if live_vm == None:
            print('Failed to find live_vm.', file=sys.stderr)
            exit(1)
        if maintenance_vm == None:
            print('Failed to find maintenance vm.', file=sys.stderr)
            exit(1)

        mstate, mreason = maintenance_vm.state()
        if mstate == libvirt.VIR_DOMAIN_PAUSED:
            print("vm is paused")
        if (mstate == libvirt.VIR_DOMAIN_RUNNING) or (mstate == libvirt.VIR_DOMAIN_PAUSED):


            if live_vm.isActive():
                live_vm.detachDevice(passthrough_device.toxml())
                live_vm.detachDevice(interface_device.toxml())
                print("detached live devices.")
                # vm has no connection anymore, vnf is down

            # attach devices to maintenance
            if mstate == libvirt.VIR_DOMAIN_PAUSED:
                print("maintenance machine is pause, start..")
                maintenance_vm.resume()

            while live_vm.isActive() != 1:
                print("live vm not active yet...")
                #time.sleep(5)

            if maintenance_vm.isActive():
                print("maintenance machine is running, attach..")
                maintenance_vm.attachDevice(passthrough_device.toxml())
                maintenance_vm.attachDevice(interface_device.toxml())
                print("attached devices.")
            # devices are now connected to maintenance vm
            # vnf is active again over the maintenance vm
        else:
            print('Maintenance VM is not running and not suspended', file=sys.stderr)
            print('reason is', str(mreason))
            exit(1)

def commit(name, config_path):
    print("detach defined defices")
    config = minidom.parse(config_path)
    maintenance_xml_path = config.getElementsByTagName("maintenance_vnf_xml_path")[0].firstChild.data
    passthrough_device = config.getElementsByTagName("passthrough_devices")[0].firstChild
    passthrough_device_name = config.getElementsByTagName("passthrough_device_name")[0].firstChild.data
    interface_device_name = config.getElementsByTagName("interface_device_name")[0].firstChild.data
    # takes only the first interface right now:
    interface_device = config.getElementsByTagName("interface_devices")[0].firstChild

    with Qemu_Connection(QEMU_USER, QEMU_PW) as qconn:
        live_vm = qconn.conn.lookupByName(name)
        maintenance_vm = qconn.conn.lookupByName(name + "-maintenance")
        if live_vm == None:
            print('Failed to find live_vm.', file=sys.stderr)
            exit(1)
        if maintenance_vm == None:
            print('Failed to find maintenance vm.', file=sys.stderr)
            exit(1)

        lstate, lreason = live_vm.state()
        if lstate == libvirt.VIR_DOMAIN_PAUSED:
            print("vm is paused")
        if (lstate == libvirt.VIR_DOMAIN_RUNNING) or (lstate == libvirt.VIR_DOMAIN_PAUSED):

            if maintenance_vm.isActive():
                maintenance_vm.detachDevice(passthrough_device.toxml())
                maintenance_vm.detachDevice(interface_device.toxml())
                print("detached maintenance devices.")
                # suspend maintenance_vm
                if maintenance_vm.suspend() < 0:
                    print('Unable to suspend guest', file=sys.stderr)
                else:
                    print("maintenance vm suspended")
                # m vm has no connection anymore, vnf is down

            # attach devices to maintenance
            if lstate == libvirt.VIR_DOMAIN_PAUSED:
                print("live machine is pause, start..")
                live_vm.resume()
                #time.sleep(1)

            while live_vm.isActive() != 1:
                print("live vm not active yet...")
                #time.sleep(5)

            if live_vm.isActive():
                print("live machine is running, attach...")
                live_vm.attachDevice(passthrough_device.toxml())
                live_vm.attachDevice(interface_device.toxml())
                print("attached devices.")
            # devices are now connected to live vm
            # vnf is active again over the live vm


        else:
            print('Live VM is not running and not suspended', file=sys.stderr)
            print('reason is', str(lreason))
            exit(1)

def send_cmd(command):
    #print(command)
    args = shlex.split(command)
    process = subprocess.run(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    state = process.returncode
    if state < 0:
        return False
    else:
        return True
