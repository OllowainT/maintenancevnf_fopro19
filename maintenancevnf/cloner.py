import os
import sys
import subprocess
import libvirt
import shlex
import sysrsync

from maintenancevnf.maintenance_configurator import Maintenance_Configurator

class Cloner(object):

    def __init__(self, live_img_path, use_old_image, name, qemu_connection, interface_device, passthrough_device):
        self.vnf_name = name
        self.conn = qemu_connection
        self.name = "cloner"
        self.live_image_path = live_img_path
        self.live_image_size = None
        self.enoughspace = False
        self.available_space = None
        self.access_perms = False
        self.backup_path_perms = False
        self.maintenance_access_perms = None
        self.ready_for_cloning = False
        self.correct_file_extension = None
        self.use_old_backup = use_old_image
        self.maintenance_image_path = None
        self.interface_device = interface_device
        self.passthrough_device = passthrough_device
        self.datastore_path = "/datastore/iso"

        #we assume, that the config path is the same as the image path
        self.live_image_xml = None

    def __enter__(self):
        #print("cloner enter")

        # self.check_access_perms()
        try:
            self.live_image_size = os.path.getsize(self.live_image_path)
        except OSError:
            print("there was an error while getting live image file size")
            raise

        # set vars
        self._set_maintenance_image_path()

        # if the argument --use-old-image is given. check if its there
        if self.use_old_backup:
            try:
                if not os.path.exists(self.maintenance_image_path):
                    raise FileNotFoundError()
            except OSError:
                print("cant find maintenance image path. Note: Backup image"
                " has the same name as live_vm with trailing *.maintenance")
                raise

        #set live image xml path:
        self.live_image_xml = self.live_image_path.split(".")[0] + ".xml"

        return self

    def __exit__(self, exception_type, exception_value, traceback):
        print("exit cloner")
        # todo: close

    def _set_maintenance_image_path(self):
        self.maintenance_image_path = str(os.path.dirname(self.live_image_path) + "/"
            + os.path.basename(self.live_image_path).split(".")[0] + "_maintenance" + ".qcow2")

    #check if the system is configured to get a clone of live-vm
    def check_system(self):

        if self.check_space():
            self.enoughspace = True
        else:
            raise SpaceLowException()

        if self.check_access_perms():
            self.access_perms = True
        else:
            raise NoAccessException()

        if self.check_file_extension():
            self.correct_file_extension = True
        else:
            raise UnsupportedFileException()

        if self.check_backup_path_access_perms():
            self.backup_path_perms = True
        else:
            raise NoAccessException()

        if self.use_old_backup:
            if self.check_backup_access_perms():
                self.maintenance_access_perms = True
            else:
                raise NoAccessException()


        all_checks = list((
            self.access_perms,
            self.check_space,
            self.correct_file_extension,
            self.maintenance_access_perms,
            self.backup_path_perms))
        if False not in all_checks:
            self.ready_for_cloning = True

        return self.ready_for_cloning

    def check_file_extension(self):
        if os.path.splitext(self.live_image_path)[1] != ".qcow2":
            return False
        else:
            return True

    def check_space(self):
        df = subprocess.Popen(["df", "%s" % self.live_image_path, "-B1"], stdout=subprocess.PIPE)
        output = df.communicate()[0]
        device, size, used, available, percent, mountpoint = \
            output.decode().split("\n")[1].split()
        self.available_space = available
        if self.live_image_size + 200 >= int(available):
            return False
        else:
            return True

    def check_for_old_backup(self):
        backup_path = self.live_image_path.split(".")[0] + "_maintenance" + ".qcow2"
        if (os.path.exists(backup_path)):
            return True
        else:
            return False


    def check_access_perms(self):
        perms = os.access(self.live_image_path, os.R_OK)
        #print("perms: ", perms)
        #self.access_perms = perms
        return perms

    def check_backup_access_perms(self):
        perms = os.access(self.maintenance_image_path, os.W_OK)
        return perms

    def check_backup_path_access_perms(self):
        image_dir = os.path.dirname(self.live_image_path)
        perms = os.access(image_dir, os.W_OK)
        return perms

    def write_config(self):
        print("try to write needed Config...")
        # todo: be more defensive
        with Maintenance_Configurator(
            self.live_image_xml,
            self.vnf_name,
            self.conn,
            self.live_image_path) as mc:

            # todo: only if the devices are given
            mc.add_interface_device(self.interface_device)
            mc.add_passthrough_device(self.passthrough_device)
            print("Config written")
        if mc:
            return True
        else:
            return False


    def clone_image(self):
        print("start cloning process...")
        # start cloning!
        live_vm = self.conn.lookupByName(self.vnf_name)

        command = "virsh snapshot-create-as --domain live-router temp_snapshot --diskspec vda,file={ds}/temp_snapshot.qcow2 --disk-only --atomic --no-metadata".format(ds=self.datastore_path)
        snap = self._send_cmd(command)
        if snap:
            print("snapshot created")

            rsync = sysrsync.run(source=self.live_image_path,
                destination=self.maintenance_image_path,
                options=['-a', '-v', '-h', '-W', '--progress'])

            if rsync.returncode != 0:
                return False
            else:
                # blockcommit to base
                bc_command = "virsh blockcommit {live_router} vda --active --verbose --pivot".format(live_router=self.vnf_name)
                bc = self._send_cmd(bc_command)
                # remove temp snap
                if bc:
                    remove_command = "rm {snap_path}/temp_snapshot.qcow2".format(snap_path=self.datastore_path)
                    if self._send_cmd(remove_command):
                        return True
                    else:
                        return False
                else:
                    return False
        else:
            print("error while creating snapshot")

    def _send_cmd(self, command):
        #print(command)
        args = shlex.split(command)
        process = subprocess.run(args, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        state = process.returncode
        if state < 0:
            return False
        else:
            return True




    def detach_from_live(self):
        print("todo: implement")

    def attach_to_live(self):
        print("todo: implement")

    def detach_from_maintenance(self):
        print("todo: implement")

    def attach_to_maintenance(self):
        print("todo: implement")

class MyException(Exception):
    pass


class SpaceLowException(MyException):
    def __init__(self):
        super().__init__()


class NoAccessException(MyException):
    def __init__(self):
        super().__init__()


class UnsupportedFileException(MyException):
    def __init__(self):
        super().__init__()
